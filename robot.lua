local component = require('component')
local sides = require('sides')
local os = require('os')
-- local math = require('math')
local computer = require('computer')

local robot = component.robot
local inv = component.inventory_controller
-- local db = component.database
-- local eth0 = component.internet
local redstone = component.redstone -- primary redstone device
-- local modem = component.tunnel -- primary linked card
local sleep = os.sleep

local CRITICAL_POWER_LEVEL = 1500 -- energy level when charging is needed
local POS_X, POS_Y, POS_Z = 0, 0, 0
local DIRECTION = 0 -- start pos 0, value changes clockwise (0 - 3)

-- function notify(str)
--    modem.send(str)
-- end

function print_pos()
    local string
    if DIRECTION == 0 then
        string = '+x'
    elseif DIRECTION == 1 then
        string = '+y'
    elseif DIRECTION == 2 then
        string = '-x'
    elseif DIRECTION == 3 then
        string = '-y'
    end

    print("x: "..POS_X..", y: "..POS_Y..", z: "..POS_Z..", direction: "..string)
end

function change_direction(bool)
    -- set local variable `direction` depend of selected rotation
    local value
    if bool == true then
       value = 1
    elseif bool == false then
        value = -1
    else
        error("give me true or false!")
    end
    DIRECTION = (DIRECTION + value) % 4
end

function move(direction, times)
    local times = times or 1

    for i=1,times do
        if computer.energy() <= CRITICAL_POWER_LEVEL then
            charge_robot()
        end
        _move(direction)
    end
end

function _move(direction)
    -- main function for moving robot
    -- function make sure that move operation was sucessful
    -- if not then robot tries to clear path with his equip tool (give him a pickaxe or something)
    local tries = 0
    local move_status, move_type, swing_status, swing_type

    move_status, move_type = robot.move(direction)

    if move_status == nil then
        robot.setLightColor(0xff0000)
        if direction == sides.back then
            robot.turn(true)
            robot.turn(true)
            swing_status, swing_type = robot.swing(sides.front)
            move_status = robot.move(sides.front)
            if swing_status == false or swing_type == "entity" then
                while move_status == nil do
                    sleep(0.5)
                    robot.swing(sides.front)
                    move_status = robot.move(sides.front)
                end
            end
            robot.setLightColor(0x00ff00)
            robot.turn(false)
            robot.turn(false)
        else
            swing_status, swing_type = robot.swing(direction)
            move_status = robot.move(direction)
            if swing_status == false or swing_type == "entity" then
                while move_status == nil do
                    sleep(0.5)
                    robot.swing(direction)
                    move_status = robot.move(direction)
                end
            end
            robot.setLightColor(0x00ff00)
        end
    end

    -- g;obal position variable update
    if direction == sides.up then
        POS_Z = POS_Z + 1
    elseif direction == sides.down then
        POS_Z = POS_Z - 1
    elseif direction == sides.front then
        if DIRECTION == 0 then
            POS_X = POS_X + 1
        elseif DIRECTION == 1 then
            POS_Y = POS_Y + 1
        elseif DIRECTION == 2 then
            POS_X = POS_X - 1
        elseif DIRECTION == 3 then
            POS_Y = POS_Y - 1
        end
    elseif direction == sides.back then
        if DIRECTION == 0 then
            POS_X = POS_X - 1
        elseif DIRECTION == 1 then
            POS_Y = POS_Y - 1
        elseif DIRECTION == 2 then
            POS_X = POS_X + 1
        elseif DIRECTION == 3 then
            POS_Y = POS_Y + 1
        end
    end
    -- temporary debug purpose
    print_pos()
end

function turn(direction)
    local status
    if direction == sides.left then
        status = robot.turn(false)
        if status then
            change_direction(false)
        end
    elseif direction == sides.right then
        status = robot.turn(true)
        if status then
            change_direction(true)
        end
    else
        error('Are you retarted? You can only turn left or right, please give proper int side value')
    end
end

function get_energy()
    local max = computer.maxEnergy()
    local current = computer.energy()
    return current / max * 100
end

function find(label) --  todo: make function not case-sensitive
    -- returns InventoryID and volume with given string label
    -- returns 0 and nil if item was not found
    local inventory_size = robot.inventorySize()
    local item

    for slot=1,inventory_size do
        item = inv.getStackInInternalSlot(slot)
        if item and item.label == label then
            return slot, item.size
        end
    end
    return 0, nil
end

function charge_robot()
    -- destroy two blocks below and put configured dimensional transceiver (EnderIO) with charger (OpenComputers)
    -- pick these blocks after full charge and back to start position
    -- return true after full charge or return false when charge is impossible (needed block is missing)
    local transceiver_slot = find("Dimensional Transceiver (Configured)")
    local charger_slot = find("Charger")
    if (transceiver_slot == 0 or charger_slot == 0) then
        return false
    end

    _move(sides.up)
    _move(sides.up)
    _move(sides.down)
    robot.select(transceiver_slot)
    robot.place(sides.up)
    _move(sides.down)
    robot.select(charger_slot)
    robot.place(sides.up)
    redstone.setOutput(sides.up, 15)
    robot.setLightColor(0x0000ff)
    while get_energy() < 99 do
        sleep(1)
    end
    robot.setLightColor(0x00ff00)
    redstone.setOutput(sides.up, 0)
    _move(sides.up)
    _move(sides.up)
    _move(sides.down)
    _move(sides.down)
    return true
end

function place_landmark(landmark_slot, block_slot)
    robot.select(landmark_slot)
    local status = robot.place(sides.down)
    if not status then
        move(sides.down, 2)
        move(sides.up)
        robot.select(block_slot)
        while not robot.place(sides.down) do
            sleep(1)
        end
        move(sides.up)
        robot.select(landmark_slot)
        while not robot.place(sides.down) do
            sleep(1)
        end
    end
end


function build_quarry(size_x, size_y, quarry_pos)
    -- build quarry in the front of the robot,
    --
    local quarry_pos = quarry_pos or 1

    if (size_x < 3 or size_y < 3) or (size_x > 64 or size_y > 64) then
        error("Minimum quarry space is 3x3, maximum is 64x64")
    end

    if quarry_pos > size_x or quarry_pos < 1 then
        error("Selection of Quarry position depend of size_x, range is from 1 to size_x")
    end

    local q_slot, q_size = find("Quarry")
    local m_slot, m_size = find("Land Mark")
    local b_slot, b_size = find("Dirt") -- support block for placing Land Mark purpose
    local d_slot = find("Dimensional Transceiver (Configured)")

    if (q_slot == 0 or m_slot == 0 or b_slot == 0 or d_slot == 0) or (q_size < 1 or m_size < 3 or b_size < 3) then
        return false
    end

    move(sides.up)
    move(sides.front, 2)
    place_landmark(m_slot, b_slot)
    move(sides.front, size_y - 1)
    place_landmark(m_slot, b_slot)
    turn(sides.right)
    move(sides.front, size_x - 1)
    place_landmark(m_slot, b_slot)
    turn(sides.right)
    turn(sides.right)
    move(sides.front, size_x - 1)
    local use_status = robot.use(sides.down)
    if not use_status then
        error('Something went wrong...')
    end
    turn(sides.left)
    move(sides.front, size_y + 1)
    move(sides.down)
    turn(sides.right)
    turn(sides.right)

    if quarry_pos > 1 then
        turn(sides.right)
        move(sides.front, quarry_pos - 1)
        turn(sides.left)
    end

    move(sides.front)
    move(sides.up)
    move(sides.back)
    robot.select(d_slot)
    while not robot.place(sides.front) do
        sleep(1)
    end
    robot.move(sides.down)
    robot.select(q_slot)
    while not robot.place(sides.front) do
        sleep(1)
    end
    return true
end

robot.setLightColor(0x00ff00)

build_quarry(8, 4, 8)